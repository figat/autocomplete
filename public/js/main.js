const inP = document.getElementsByClassName("search-text")[0];
const result = document.getElementsByClassName("result")[0];
const close = document.getElementsByClassName("close")[0];
const new_list = document.getElementsByClassName("new_list")[0];
let names = [];
inP.focus();

inP.addEventListener("click", function(e) {
  result.innerHTML = "";
  result.classList.add("list");
  close.classList.toggle("ds");
  createList(names);
});

close.addEventListener("click", function(e) {
  e.target.classList.remove("ds");
  result.classList.remove("list");
  inP.value = "";
});

inP.addEventListener("keyup", function(e) {
  e.preventDefault();
  e.stopPropagation();

  result.innerHTML = "";
  const value = e.target.value;
  const regexp = new RegExp(value);
  const newValues = names.filter(v => v.match(regexp));

  createList(newValues);
});

fetch(`http://localhost:8080/autocomplite`)
  .then(res => {
    return res.json();
  })
  .then(data => {
    const { allNames } = data;
    for (let i = 0; i < allNames.length; i++) {
      names.push(allNames[i]);
    }
  })
  .catch(err => {
    console.log(err);
  });

function createList(items) {
  items.map(item => {
    let listItems = document.createElement("li");
    listItems.classList.add("new_list");
    listItems.textContent = item;
    result.appendChild(listItems);
  });
}

result.addEventListener("click", function(e) {
  close.classList.add("ds");
  inP.value = e.target.textContent;
});
